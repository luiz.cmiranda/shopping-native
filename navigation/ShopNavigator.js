
import * as React from "react";
import { View, Text } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { ProductsOverview } from "../screens/shop/ProductsOverview";
import ProductDetailScreen from "../screens/shop/ProductDetailScreen";

const Stack = createNativeStackNavigator();

function ShopNavigator() {
  return (
    <NavigationContainer initialRouteName="All Products">
      <Stack.Navigator>
        <Stack.Screen name="All Products" component={ProductsOverview} />
        <Stack.Screen name="Detail Products" component={ProductDetailScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default ShopNavigator;
